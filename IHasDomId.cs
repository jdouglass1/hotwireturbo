﻿namespace HotwireTurbo {
    public interface IHasDomId {
        string ToDomId();
    }
}